# kbot
devops application from scratch

## link to rep:<br>
[https://gitlab.com/Setiuss/kbot](https://gitlab.com/Setiuss/kbot)<br>
[https://gitlab.com/Setiuss/secops](https://gitlab.com/Setiuss/secops)<br>

## test to secrets:
[https://gitlab.com/Setiuss/kbot/-/tree/test/scanner-secrets/helm?ref_type=heads](https://gitlab.com/Setiuss/kbot/-/tree/test/scanner-secrets/helm?ref_type=heads)<br>
[https://gitlab.com/Setiuss/secops/-/tree/feature/secrets-scan/templates?ref_type=heads](https://gitlab.com/Setiuss/secops/-/tree/feature/secrets-scan/templates?ref_type=heads)<br>


*cli ot manual start:*

- go mod init github.com/Setiuss/kbot

- go install github.com/spf13/cobra-cli@latest

- cobra-cli init

- cobra-cli add version

- cobra-cli add kbot

- gofmt -s -w ./

- go get

- go build -ldflags "-X="github.com/Setiuss/kbot/cmd.appVersion=v1.0.0

- read -s TELE_TOKEN

- echo $TELE_TOKEN

- export TELE_TOKEN

- go build -ldflags "-X="github.com/Setiuss/kbot/cmd.appVersion=v1.0.1

- ./kbot start


Telegram bot:<br>
t.me/setius1_bot

Command:<br>
/start hello<br>
/start Hello<br>
/start How are You?<br>
/start how are you?<br>
